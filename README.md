# EXPORT YOUR AWS_ACCESS_KEY_ID

export AWS_ACCESS_KEY_ID="<Access key>"

# EXPORT YOUR AWS_SECRET_ACCESS_KEY_ID

export AWS_SECRET_ACCESS_KEY="<Secret access key>"
