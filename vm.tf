data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

}

resource "aws_key_pair" "terraform-key-pair" {
  key_name   = "terraform-ssh-key"
  public_key = file("./aws-access.pub")
}

resource "aws_instance" "terraform-vm" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.terraform-subnet.id
  vpc_security_group_ids = [aws_security_group.terraform-security-group.id]
  key_name               = aws_key_pair.terraform-key-pair.key_name
  associate_public_ip_address = true

  tags = {
    Name = "terraform-vm"
  }

}
